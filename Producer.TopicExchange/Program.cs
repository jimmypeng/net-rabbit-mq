﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Producer.TopicExchange
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    string exchangeName = "Topic_exchange";

                    channel.QueueDeclare("hello1", false, false, false, null);
                    channel.QueueDeclare("hello2", false, false, false, null);

                    channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, false, false, null);

                    channel.QueueBind("hello1", exchangeName, "routekey.*");
                    channel.QueueBind("hello2", exchangeName, "routekey.#");
                    channel.QueueBind("hello2", exchangeName, "routekey.one");

                    Console.WriteLine("\nRabbitMQ连接成功，请输入消息，输入exit退出！");

                    string input;
                    do
                    {
                        input = Console.ReadLine();

                        var sendBytes = Encoding.UTF8.GetBytes(input);

                        channel.BasicPublish(exchangeName, "routekey.two.one", null, sendBytes);
                    }
                    while (input.Trim().ToLower() != "exit");
                }
            }
        }
    }
}
