﻿using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ConsumerSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost"
            };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (ch, ea) =>
                    {
                        var message = Encoding.UTF8.GetString(ea.Body.ToArray());
                        Console.WriteLine($"收到消息：{message}");

                        // 确认该消息已被消费
                        channel.BasicAck(ea.DeliveryTag, false);
                    };

                    channel.BasicConsume("hello2", false, consumer);
                    Console.WriteLine("消费者2已启动");
                    Console.ReadKey();
                }

            }
        }
    }
}
