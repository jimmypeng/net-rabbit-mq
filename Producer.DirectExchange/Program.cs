﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Producer.DirectExchange
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    string exchangeName = "direct_exchange";

                    channel.QueueDeclare("hello1", false, false, false, null);
                    channel.QueueDeclare("hello2", false, false, false, null);

                    channel.ExchangeDeclare(exchangeName, ExchangeType.Direct, false, false, null);

                    channel.QueueBind("hello1", exchangeName, "orange");
                    channel.QueueBind("hello2", exchangeName, "green");
                    channel.QueueBind("hello2", exchangeName, "black");

                    Console.WriteLine("\nRabbitMQ连接成功，请输入消息，输入exit退出！");

                    string input;
                    do
                    {
                        input = Console.ReadLine();

                        var sendBytes = Encoding.UTF8.GetBytes(input);

                        channel.BasicPublish(exchangeName, "black", null, sendBytes);
                    }
                    while (input.Trim().ToLower() != "exit");
                }
            }
        }
    }
}
