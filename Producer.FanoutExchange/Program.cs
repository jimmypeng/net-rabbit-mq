﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Producer.FanoutExchange
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    string exchangeName = "fanout_exchange";

                    channel.QueueDeclare("hello1", false, false, false, null);
                    channel.QueueDeclare("hello2", false, false, false, null);

                    channel.ExchangeDeclare(exchangeName, ExchangeType.Fanout, false, false, null);

                    channel.QueueBind("hello1", exchangeName, "");
                    channel.QueueBind("hello2", exchangeName, "");
                    channel.QueueBind("hello2", exchangeName, "");

                    Console.WriteLine("\nRabbitMQ连接成功，请输入消息，输入exit退出！");

                    string input;
                    do
                    {
                        input = Console.ReadLine();

                        var sendBytes = Encoding.UTF8.GetBytes(input);

                        channel.BasicPublish(exchangeName, "", null, sendBytes);
                    }
                    while (input.Trim().ToLower() != "exit");
                }
            }
        }
    }
}
