﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace RabbitMQGetStarted
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare("hello1", false, false, false, null);

                    Console.WriteLine("\nRabbitMQ连接成功，请输入消息，输入exit退出！");

                    string input;
                    do
                    {
                        input = Console.ReadLine();

                        var sendBytes = Encoding.UTF8.GetBytes(input);

                        channel.BasicPublish("", "hello1", null, sendBytes);
                    }
                    while (input.Trim().ToLower()!="exit");
                }
            }
        }
    }
}
